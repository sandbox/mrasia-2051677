/**
 * @file
 * Ace editor settings.
 */

(function($) {
  Drupal.behaviors.editr = {
    attach: function(context, settings) {
       // var ace = "/sites/all/libraries/ace";
        //var path = "/sites/all/libraries/ace";
        //ace.config.set("workerPath", path);
        //var editor = ace.edit("editr");
        //editor.setTheme("ace/theme/monokai");
        //editor.session.setMode("ace/mode/css");
      $('.editr').editr();
    }
  };
})(jQuery);